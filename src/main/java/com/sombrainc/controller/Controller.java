package com.sombrainc.controller;

import com.sombrainc.config.Configuration;
import com.sombrainc.enumeration.Role;
import com.sombrainc.model.User;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class Controller {

    private final ObservableList<User> users = FXCollections.observableArrayList();

    @FXML
    private TableView<User> userTableView;

    @FXML
    private TableColumn<User, String> firstNameColumn;

    @FXML
    private TableColumn<User, String> lastNameColumn;

    @FXML
    private TableColumn<User, Role> roleColumn;

    @FXML
    private TableColumn<User, Integer> counterColumn;

    @FXML
    public void initialize() {
        firstNameColumn.setCellValueFactory(C -> C.getValue().firstNameProperty());
        lastNameColumn.setCellValueFactory(C -> C.getValue().lastNameProperty());
        roleColumn.setCellValueFactory(C -> C.getValue().roleProperty());
        counterColumn.setCellValueFactory(C -> C.getValue().counterProperty().asObject());
        userTableView.setItems(users);

        counterColumn.setCellFactory(C -> new TableCell<User, Integer>() {
            @Override
            public void updateItem(final Integer value, final boolean empty) {
                super.updateItem(value, empty);

                setText(empty ? "" : getItem().toString());

                if (!empty && value > Integer.parseInt(Configuration.PROPERTIES.getProperty("ALTERNATIVE_COLOR_COUNT"))) {
                    getTableRow().setStyle("-fx-background-color: red;");
                } else {
                    getTableRow().setStyle("");
                }
            }
        });
    }

    public void handleAddButton() {
        Platform.runLater(() -> {
            for (int i = 0; i < Integer.parseInt(Configuration.PROPERTIES.getProperty("NEW_OBJECTS_COUNT")); i++) {
                users.add(User.random());
            }
        });
    }

    public void handleModifyButton() {
        Platform.runLater(() -> users.forEach(User::incrementCounter));
    }

}
