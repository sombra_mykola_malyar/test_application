package com.sombrainc.model;

import com.sombrainc.config.Configuration;
import com.sombrainc.enumeration.Role;
import com.sombrainc.util.RandomStringGenerator;
import javafx.beans.property.*;

public class User {

    private StringProperty firstName;
    private StringProperty lastName;
    private ObjectProperty<Role> role;
    private IntegerProperty counter;

    public static User random() {
        return new User(
                RandomStringGenerator.generateString(Integer.parseInt(Configuration.PROPERTIES.getProperty("FIRST_NAME_LENGTH"))),
                RandomStringGenerator.generateString(Integer.parseInt(Configuration.PROPERTIES.getProperty("LAST_NAME_LENGTH"))),
                Role.values()[(int) (Math.random() * Role.values().length)]
        );
    }

    public User(final String firstName, final String lastName, final Role role) {
        this.firstName = new SimpleStringProperty(firstName);
        this.lastName = new SimpleStringProperty(lastName);
        this.role = new SimpleObjectProperty<>(role);
        this.counter = new SimpleIntegerProperty(0);
    }

    public String getFirstName() {
        return firstName.get();
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public String getLastName() {
        return lastName.get();
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public Role getRole() {
        return role.get();
    }

    public ObjectProperty<Role> roleProperty() {
        return role;
    }

    public int getCounter() {
        return counter.get();
    }

    public IntegerProperty counterProperty() {
        return counter;
    }

    public void incrementCounter() {
        counter.setValue(counter.get() + 1);
    }
}
