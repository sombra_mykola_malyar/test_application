package com.sombrainc.util;

public class RandomStringGenerator {

    private RandomStringGenerator() {
        throw new UnsupportedOperationException();
    }

    private final static String BASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    public static String generateString(final int length) {
        final StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            final int charAt = (int) (Math.random() * BASE.length());
            stringBuilder.append(BASE.charAt(charAt));
        }
        return stringBuilder.toString();
    }
}
