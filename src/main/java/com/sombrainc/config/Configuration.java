package com.sombrainc.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Configuration {

    public static final Properties PROPERTIES;

    private Configuration() {
        throw new UnsupportedOperationException();
    }

    static {

        final Properties loadedProp = new Properties();
        try (final InputStream resource = Configuration.class.getClassLoader().getResourceAsStream("application.properties")) {
            loadedProp.load(resource);
        } catch (final IOException e) {
            e.printStackTrace();
        }

        PROPERTIES = loadedProp;
    }

}
