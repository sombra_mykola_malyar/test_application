package com.sombrainc.enumeration;

public enum Role {

    DEVELOPER("Developer"),
    HR("HR"),
    MANAGER("Manager"),
    SALES("Sales");

    private final String title;

    Role(final String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
