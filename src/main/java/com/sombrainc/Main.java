package com.sombrainc;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(final Stage stage) throws Exception {
        final Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("application.fxml"));
        stage.setTitle("Sombra");
        stage.setScene(new Scene(root, 800, 600));
        stage.show();
    }


    public static void main(final String[] args) {
        launch(args);
    }
}
