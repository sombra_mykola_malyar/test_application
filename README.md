**Tools needed to build the application:**
=====

1)JDK v1.8

2)Apache Maven

**How to build:**
=====

1)Navigate to the project root folder

2)Execute 

```
mvn:jfx-jar
```

**How to run:**
=====

1)Navigate to 

```
[PROJECT_ROOT]/target/jfx/app
```

2)Execute 

```
java -jar application-1.0-SNAPSHOT-jfx.jar
```